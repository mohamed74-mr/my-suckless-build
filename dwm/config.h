/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx        = 3;   /* border pixel of windows */
static const unsigned int snap            = 32;  /* snap pixel */
static const unsigned int systraypinning  = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft   = 0;   /* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing  = 0;   /* systray spacing */
static const int systraypinningfailfirst  = 0;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const int showbar            = 1;     /* 0 means no bar */
static const int topbar             = 1;     /* 0 means bottom bar */
static const int focusonwheel       = 0;
static const int user_bh            = 32;    /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const unsigned int gappx     = 13;    /* gaps between windows */
static const char *fonts[]          = { "Courier 10 Pitch:size=13", "FontAwesome:size=16"};
static const char dmenufont[]       = "Courier 10 Pitch:size=14";
static const char col_gray1[]       = "#121212";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#434343";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, "#708090" },
	[SchemeSel]  = { col_gray4, col_cyan,  "#F0F8FF" },
};

/* tagging */
/* static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" }; */
static const char *tags[] = { "", "", "", "", "", "", "", "", "" };
static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Rxvt",     NULL,       NULL,       1 << 1,       0,           -1 },
	{ "st",       NULL,       NULL,       1 << 1,       0,           -1 },
	{ "Emacs",    NULL,       NULL,       1 << 2,       0,           -1 },
	{ "Thunar",   NULL,       NULL,       1 << 4,       0,           -1 },
	{ "Zathura",  NULL,       NULL,       1 << 3,       0,           -1 },
	{ "Atril",    NULL,       NULL,       1 << 3,       0,           -1 },
	{ "gwenview", NULL,       NULL,       1 << 7,       0,           -1 },
	{ "okular",   NULL,       NULL,       1 << 3,       0,           -1 },
	{ "firefox",  NULL,       NULL,       1 << 6,       0,           -1 },
	{ "Falkon",   NULL,       NULL,       1 << 5,       0,           -1 },
	{ "qutebrowser",NULL,     NULL,       1 << 5,       0,           -1 },
	{ "Surf",     NULL,       NULL,       1 << 5,       0,           -1 },
	{ "Telegram", NULL,       NULL,       1 << 6,       0,           -1 },	
	{ "vlc",      NULL,       NULL,       1 << 7,       0,           -1 },
	{ "Gimp",   NULL,         NULL,       1 << 7,       0,           -1 },
	{ "virtualbox",   NULL,         NULL,       1 << 8,       0,           -1 },
	/* { "urxvt",    NULL,       NULL,       1 << 0,       0,           -1 }, */
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */


/* #include "tcl.c" */
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "  ",      tile },    /* first entry is default */
	{ "  ",      NULL },    /* no layout function means floating behavior */
	{ "  ",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask

#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
/* static const char *termcmd[]  = { "urxvt", NULL }; */
static const char *termcmd[]  = { "st", NULL };
/* custom apps commands */
static const char *emacs[]     = { "emacs", NULL };
static const char *firefox[]   = { "firefox", NULL };
static const char *falkon[]    = { "falkon", NULL };
static const char *surf[]      = { "surf", NULL };
static const char *qute[]      = { "qutebrowser", NULL };
static const char *thunar[]    = { "thunar", NULL };
static const char *zathura[]   = { "zathura", NULL };
static const char *telegram[]  = { "telegram-desktop", NULL };
static const char *vlc[]       = { "vlc", NULL };
static const char *okular[]     = { "okular", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_d,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_e,      spawn,          {.v = emacs } },
	{ MODKEY,                       XK_f,      spawn,          {.v = firefox } },
	{ MODKEY,                       XK_w,      spawn,          {.v = falkon } },
	{ MODKEY,                       XK_s,      spawn,          {.v = surf } },
	{ MODKEY,                       XK_t,      spawn,          {.v = thunar } },
	{ MODKEY,                       XK_z,      spawn,          {.v = zathura } },
	{ MODKEY,                       XK_q,      spawn,          {.v = qute } },
	{ MODKEY,                       XK_v,      spawn,          {.v = vlc } },
	{ MODKEY,                       XK_g,      spawn,          {.v = telegram } },
	{ MODKEY|ShiftMask,             XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_Right,  focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_Left,   focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_Down,   focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_Up,     focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_Right,  movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_Left,   movestack,      {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY|Mod1Mask,             XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY|Mod1Mask,             XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY|Mod1Mask,             XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY|ShiftMask,             XK_f,      fullscreen,     {0} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)

	{ MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },

	{ MODKEY|ControlMask,           XK_Right,            view_adjacent,  { .i = +1 } },
	{ MODKEY|ControlMask,           XK_Left,             view_adjacent,  { .i = -1 } },
	
	{ MODKEY|ControlMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
